<form class="row g-3" method="POST">
  <div class="col-md-6">
    <?php if (isset($data['id'])) : ?>

    <input type="hidden" name="id" value="<?=isset($data['id']) ?  $data['id'] : ""?>"
    <?php endif ?>
    
    <label for="inputNama" class="form-label">Nama Barang</label>
    <input name="name" type="text" class="form-control" id="inputNama" aria-describedby="namaHelp" value="<?=isset($data['nama']) ?  $data['nama'] : ""?>">
    <small id="namaHelp" class="form-text text-muted">Isikan nama barang</small>
    
    <label for="inputJumlah" class="form-label">Jumlah Barang</label>
    <input name="qty" type="text" class="form-control" id="inputJumlah" aria-describedby="jumlahHelp" value="<?=isset($data['qty']) ?  $data['qty'] : ""?>">
    <small id="jumlahHelp" class="form-text text-muted">Isikan jumlah barang</small>
  </div>
  <div class="col-12">
    <button type="submit" class="btn btn-primary">Simpan</button>
  </div>
</form>